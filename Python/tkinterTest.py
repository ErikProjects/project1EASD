from tkinter import *
from tkinter.ttk import *

#From this link: https://likegeeks.com/python-gui-examples-tkinter-tutorial/

selected = IntVar

window = Tk()
#Window title
window.title("Welcome to LikeGeeks app")
#Window size
window.geometry('350x200')
#Creates a string in the window
lbl = Label(window, text="Hello", font=("Arial Bold", 50))
#Repositions the string
lbl.grid(column=0, row=0)
#Creates string entry
txt = Entry(window,width=10)
#Repositions the string entry
txt.grid(column=1, row=0)
#Starts in the text writing thing
txt.focus()
#Now you cant enter text
txt = Entry(window,width=10, state='disabled')
#Do something if button is clicked
""" def clicked():

    res = "Welcome to " + txt.get() + combo.get()

    lbl.configure(text= res) """
def clicked():

   print(selected.get)
#Creates button and command
btn = Button(window, text="Click Me", command = clicked)
#Positions the button
btn.grid(column=2, row=0)
#Creates a combox class from the ttk library
combo = Combobox(window)
#Sets up the values for the pop down menu
combo['values']= (1, 2, 3, 4, 5, "Text")

combo.current(1) #set the selected item
#Sets the combo class' location
combo.grid(column=3, row=0)
#Make check box's boolean variable
chk_state = BooleanVar()

chk_state.set(True) #set check state
#Make check button have a strong next to it
chk = Checkbutton(window, text='Choose', var=chk_state)
#Set check button's location
chk.grid(column=4, row=0)
#Create first radio button
rad1 = Radiobutton(window,text='First', value=1, variable=selected)
#Create second radio button
rad2 = Radiobutton(window,text='Second', value=2, variable=selected)
#Create third radio button
rad3 = Radiobutton(window,text='Third', value=3, variable=selected)
#Relocate first radio button
rad1.grid(column=0, row=1)
#Relocate second radio button
rad2.grid(column=1, row=1)
#Relocate third radio button
rad3.grid(column=2, row=1)




window.mainloop()

