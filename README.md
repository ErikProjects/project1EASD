# Project1EASD
Workout Scheduler
Use order:
0) Optional: Create a data set using the NewExercisePage.py and JsonManager.py executables.


Create a workout specialised for your own need using the NewWorkout.py executable.


Use the BlockSorter.py to create a unique schedule.


View your created schedule in the form of a .json file.


Future Development:
Algorythm:


pathFinderV1: Randomly interated through available blocks checking for failiure and escape conditions.
Set maximum number of iterations. Does not maximise efficiency.


pathFinderV2: Checks the furthest from completion value, then searches blocks which contain said variable. Iterates through blocks checking for failiure and escape conditions.
Set number of iterations. Minimises amount of blocks(exercises) used. May be random or ranked based on numbers.


pathFinderV3(Currently in development): Modified pathFinderV2 which also includes weekly failiure and escape conditions.
Currently requires reshuffling of everything previously established due to need for hierarchy, predictive measures and threading. So far it appears to be out of scope and will require full re-imagining of previously utilised concepts.


Other:


Use of external data-sets along with the GUI JsonManager.py.


Displaying workout routine to user rather than JSON file.


Implementation of more intuitive UI.


by E.V. (2022)